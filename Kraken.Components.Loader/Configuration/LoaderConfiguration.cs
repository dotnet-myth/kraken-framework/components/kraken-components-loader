﻿using BlazorPro.Spinkit;
using Kraken.Component.Loader.Interfaces;

namespace Kraken.Component.Loader.Configuration {

    public class LoaderConfiguration: ILoaderConfiguration {
        public SpinnerType Spinner { get; set; }
        public string Color { get; set; }
        public string BackgroundColor { get; set; }
        public string FullPageSize { get; set; }
        public string Size { get; set; }

        public LoaderConfiguration( SpinnerType spinner, string color, string backgroundColor, string fullPageSize, string size ) {
            Spinner = spinner;
            Color = color;
            BackgroundColor = backgroundColor;
            FullPageSize = fullPageSize;
            Size = size;
        }
    }
}