﻿using BlazorPro.Spinkit;
using Kraken.Component.Loader.Configuration;
using Kraken.Component.Loader.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Kraken.Component.Loader.Extensions {

    public static class Configuration {

        public static IServiceCollection AddLoader( this IServiceCollection services, SpinnerType spinner = SpinnerType.Circle, string color = "black", string backgroundColor = "white", string fullPageSize = "6em", string size = "30px" ) {
            ILoaderConfiguration configuration = new LoaderConfiguration( spinner, color, backgroundColor, fullPageSize, size );
            return services.AddSingleton( configuration );
        }
    }
}