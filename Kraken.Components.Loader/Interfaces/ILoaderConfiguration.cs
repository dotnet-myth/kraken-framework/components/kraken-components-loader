﻿using BlazorPro.Spinkit;

namespace Kraken.Component.Loader.Interfaces {

    public interface ILoaderConfiguration {
        string BackgroundColor { get; set; }
        string Color { get; set; }
        SpinnerType Spinner { get; set; }
        string FullPageSize { get; set; }
        string Size { get; set; }
    }
}